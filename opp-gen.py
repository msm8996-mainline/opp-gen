#!/usr/bin/python
# SPDX-License-Identifier: GPL-2.0-only

import sys
import argparse

def main(argv):
	parser = argparse.ArgumentParser(description='Generate OPP tables.')

	parser.add_argument(
		'-f', '--frequencies-file',
		nargs=1,
		type=str,
		required=True,
		help='file containing a list of opp-hz values.'
	)
	parser.add_argument(
		'-s', '--supported-hw-file',
		nargs=1,
		type=str,
		required=False,
		help='file containing a list of opp-supported-hw values.'
	)
	parser.add_argument(
		'-l', '--clock-latency',
		nargs=1,
		type=int,
		required=False,
		help='clock-latency-ns value for all OPPs.'
	)
	parser.add_argument(
		'--clock-latencies-file',
		nargs=1,
		type=str,
		required=False,
		help='file containing a list of clock-latency-ns values.'
	)

	args = parser.parse_args()

	freqs = [s.strip() for s in open(args.frequencies_file[0], 'r').readlines()]

	if args.supported_hw_file:
		supported_hws = [s.strip() for s in open(args.supported_hw_file[0], 'r').readlines()]
	else:
		supported_hws = None

	if args.clock_latencies_file:
		clock_latencies = [s.strip() for s in open(args.clock_latencies_file[0], 'r').readlines()]
	elif args.clock_latency:
		clock_latencies = [str(args.clock_latency[0]) for i in range(len(freqs))]
	else:
		clock_latencies = None

	for i in range(len(freqs)):
		if freqs[i] == "":
			break

		print("opp-%s {" % freqs[i])
		print("\topp-hz = /bits/ 64 <%s>;" % freqs[i])
		if supported_hws:
			print("\topp-supported-hw = <%s>;" % supported_hws[i])
		if clock_latencies:
			print("\tclock-latency-ns = <%s>;" % clock_latencies[i])
		print("};")

if __name__ == '__main__':
	main(sys.argv[1:])
